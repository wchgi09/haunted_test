#!/bin/bash
# install and eventualy configure tools I like to have on a Gentoo machine
#
# ufed - userflag editor, sets global flags in portage/make.conf
# getoolkit - collection of portage utilities, http://wiki.gentoo.org/wiki/Gentoolkit
# zerofree - zero out free filesystem space
# tmux - terminal multiplexer, needs config
# vim - editor of choice, needs config
# eix - portage packege search
# salt - management framework, needs to be running
# distcc - distributed compiler, needs config
# git - today's version control system, needs config
echo "Installing my needful tools..."
emerge -DNuv ufed gentoolkit zerofree tmux vim genlop eix \
	salt distcc dev-vcs/git --autounmask-write
etc-update --automode -5
emerge -DNuv ufed gentoolkit zerofree tmux vim genlop ufed eix \
	salt distcc dev-vcs/git 

#!/bin/bash
# Update https://vagrantcloud.com/search?utf8=%E2%9C%93&sort=&provider=&q=gentoo
# Vagrant Cloud: d11wtq/gentoo

######################################################
# Script to be called during vagrant up provisioning #
# stage. Takes a Gentoo Base box from Vagrant cloud  #
# and begins customizing to my tastes.               #
######################################################

# Bring in portage snapshot, convert to NFS mount if local portage repo available
echo "Bring in latest portage snapshot..."
emerge-webrsync

# Set to unstable branch
echo "set ACCEPT_KETWORDS in make.conf to swith to unstable testing branch..."
echo "ACCEPT_KEYWORDS=\"~amd64\"">>/etc/portage/make.conf

# Emerge install all updates for world...
echo "Emerging updates to world ...."
emerge -DNuv @world

# Post update cleanup steps
echo "Perl-cleaner and post world update to fixes for perl..."
# Solve for perl Blockers on perl-cleaner by removing the blocks
emerge -C ExtUtils-MakeMaker Module-CoreList version ExtUtils-CBuilder \
	ExtUtils-Manifest IO IPC-Cmd
perl-cleaner --all

# Rebuild everything which had dependencies change because of world update
echo "Emerge @preserved-rebuild, apply all config updates and read all news ..."
# Solve for ambiguous USE flag problem with man-db
echo "sys/man-db -berkdb" >>/etc/portage/package.use
emerge @preserved-rebuild
etc-update --automode -5
eselect news read all

# Change to new gcc
echo "Setting gcc compiler profile to latest version..."
gcc-config 2
. /etc/profile

#update kernel
echo "Install genkernel-next, select kernel to build, and build it..."
emerge genkernel-next
eselect kernel set 1
cd /usr/src/linux
genkernel all

# Apply GRUB settings and update boot config
#   For now, step on the kernel interface naming for nics
echo "Update GRUB2 defaults and rebuild boot config to pick up the new kernel..."
cd /etc/default
sed -i.bak 's/#GRUB_CMDLINE_LINUX=""/GRUB_CMDLINE_LINUX="net.ifnames=0"/' grub 
grub2-mkconfig -o /boot/grub/grub.cfg

# Remove any existing virtualbox guest additions installed through emerge
# Will be installing guest additions from mounted iso to stay in sync with 
# Virtualbox host.
echo "Emerge remove Virtualbox guest adittions, if any..."
emerge -C virtualbox-guest-additions

# Emerge rebuild any modules related to kernel using latest kernel
echo "Rebuild packages tied to kernel version if any..."
emerge -DNuv @module-rebuild --autounmask-write
etc-update --automode -5 
emerge -DNuv @module-rebuild

# start cleaning up cruft
echo "Remove unneeded packages and portage repository to save space..."
emerge --depclean
rm -rf /usr/portage/*

# Get vbox shared folders working after updates
echo "Installing Virtual box guet additions from mounted iso..."
mkdir /mnt/cd
mount /dev/cdrom /mnt/cd
pushd /mnt/cd
sh VBoxLinuxAdditions.run
popd
# make sure virtual box shared folder module is loaded on startup
echo 'modules="vboxsf"' >>/etc/conf.d/modules
umount /mnt/cd
rmdir /mnt/cd

# zero out space
echo "Remount root read only, and attempt to fill unallocated blocks with non-zero value content in an ext2, ext3 or ext4 file-system with zeros..."
mount -n -o remount,ro -t ext4 /dev/sda4
zerofree /dev/sda4

echo "Finis..."
